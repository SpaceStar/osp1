#!/bin/bash

exec 3>&2
exec 2>> $HOME/lab1_err

trap '' ALRM HUP INT PIPE TERM USR1 USR2 POLL PROF VTALRM

IFS=$'\n'

exit_if_fail() {
	if [ $? -ne 0 ]; then
		exit
	fi
}

error_if_fail() {
	if [ $? -ne 0 ]; then
		echo "error"
	fi
}

print_catalog() {
	pwd | gsed -r 's#.*/([^/]*)$#\1#'
}

change_catalog() {
	echo "Введите имя нового каталога"
	read -r target
	exit_if_fail
	if [ -d "$target" ]; then
		cd "$target"
		error_if_fail
	else
		echo "not a directory"
	fi
}

list_catalog() {
	ls
	error_if_fail
}

create_hard_link() {
	echo "Введите имя файла"
	read -r target
	exit_if_fail
	echo "Введите имя ссылки"
	read -r name
	exit_if_fail
	ln -- "$target" "$name"
	error_if_fail
}

remove_link() {
	echo "Введите имя файла"
	read -r target
	exit_if_fail
	if [ -L "$target" ]; then
		file=`echo "$target" | gsed -r 's#(.*/)?([^/]*)$#\2#'`
		echo "remove $file (y/n)?"
		read answer
		exit_if_fail
		if echo "$answer" | /usr/xpg4/bin/grep -Eqi '^(y|yes)$'; then
			rm -- "$target"
			error_if_fail
		fi
	else
		echo "Неверно указан целевой файл"
	fi
}

while true; do
	echo "Выберите номер действия:"
	echo "1 - Напечатать имя текущего каталога"
	echo "2 - Сменить текущий каталог"
	echo "3 - Напечатать содержимое текущего каталога"
	echo "4 - Создать прямую ссылку на файл"
	echo "5 - Удалить символическую ссылку на файл"
	echo "6 - Выйти из программы"

	read action
	exit_if_fail
	case $action in
		1) print_catalog;;
		2) change_catalog;;
		3) list_catalog;;
		4) create_hard_link;;
		5) remove_link;;
		6) exit;;
		*)
			echo "Неверное действие. Повторите ввод."
			continue;;
	esac
done
